\documentclass[12pt]{article}
\usepackage{graphicx}

\title{Congress Exposed: Technical Report 2}
\author{
  Bohan Zhang \\
  David Durairaj \\
  Dilan Galindo Reyna \\
  Souvik Banerjee \\
  Yuki Kameyama \\
}
\date{\today}

\begin{document}
{\let\newpage\relax\maketitle}


\section{Motivation}
Even though our government has been around for hundreds of years, there are still quite a lot of obscurities, especially surrounding Congress, that may be unfamiliar to the public eye. Therefore, our motivation is to unveil the curtain that lies between the Congress and the people, and in doing so, educate the general public on the inner workings of our government along with some contemporary issues. Furthermore, studies have shown that most Americans are undereducated when it comes to the inner workings of Congress, and this is especially true concerning both Congress people and bills. As a result, we wish to enlighten people on any obscure issues that Congress may have overlooked, blundered, or otherwise failed to provide an effective solution.

\section{User Stories}
\section{User Stories}
\section{Motivation}
Even though our government has been around for hundreds of years, there are still quite a lot of obscurities, especially surrounding Congress, that may be unfamiliar to the public eye. Therefore, our motivation is to unveil the curtain that lies between the Congress and the people, and in doing so, educate the general public on the inner workings of our government along with some contemporary issues. Furthermore, studies have shown that most Americans are undereducated when it comes to the inner workings of Congress, and this is especially true concerning both Congress people and bills. As a result, we wish to enlighten people on any obscure issues that Congress may have overlooked, blundered, or otherwise failed to provide an effective solution.

\section{User Stories}
We are the \textbf{customers} to the group \textit{Living Endangerously}, and our five user stories for them include:
\begin{enumerate}
  \item The frontend server should consume the RESTful API implemented in backend.
  \item Merge all of the different branches into the master branch.
  \item Add Pagination for all the model pages.
  \item Deploy changes onto cloud platform, either AWS or GCP. 
  \item Add estimated time of completion for this phase. 
\end{enumerate}
We are the \textbf{developers} to the group \textit{UnbEATable}, and they provided the following user stories:
\begin{enumerate}
  \item ``Missing middle names''
  \item ``Make Bills landing page look better''
  \item ``Consistent sizing for Twitter/Facebook attributes''
  \item ``Remove Bulletpoints inside the individual pages for Congresspeople''
  \item ``Missing images for some model instances''
\end{enumerate}
We addressed each issue through the following means:
\begin{enumerate}
  \item We also noticed that a lot of our Congresspeople don't have middle names. Thus, we resolved this by using the \textit{full name} attribute from our API, which in turn returns the full name of our Congresspeople with their middle names included. If the Congressperson does not have a middle name, we had a condition in our frontend code that checked for this and simply used an empty String for a nonexistent middle name. Our estimated time of completion for this is about 40 minutes but it actually took us an hour or so.
  \item Because each bill does not explicitly associate with a specific image, we decided to add an uniform image for all bills for better aesthetics on our landing page. We implemented this by modifying our database to include the same image for all bills when called by our API. We also modified our Javascript page to render the returned image for every bill instance. Our estimated time of implementation for this issue was around 5 minutes, and adding this in took us about 5 minutes.
  \item This is definitely a good suggestion. Instead of having the user first see a congressperson's Twitter page and then his or her Facebook page, we should put them side by side for comparison purposes. However, because the deadline is tonight and we don't want to rush this implementation. We will be resolving this issue as soon as we are done with implementing the requirements for the current phase. We plan on implementing this via some simple styling and should take approximately 30 minutes to an hour.
  \item We do not agree with this suggestion. For one, because we listed attributes via bullet points in the model's landing page, it would make sense for us to also have bullet points within each individual page, which should be extensions of the instances displayed. Furthermore, because we are reusing the same React components to display information both on the general page and the individual page, it would be consistent for us to keep the listing structure. Therefore, we have no immediate plans to remove the bullet points unless we remove every bullet points altogether
  \item This is either because the data is simply not found or the fact that the nature of our model does not include an image. We addressed this issue by either scraping more data or manually putting images into our database. For those instances that don't include images such as Bills, we provided an uniform image for all bills which matches the decor of our website. This issue took us about half an hour to resolve.
\end{enumerate}


\section{RESTful API}
In the current phase, our RESTful API is implemented using python flask and designed by Postman. The API interacts with a MySQL Database that is stored in the AWS RDS. The database contains information scrapped from our ProPublica. The four main models are: Issue, Bill, Member, Committee, and there are submodels to support the main models. The expectation of this API is, to give our front end team with well-structured JSON data, and give our customer the data we need.

\subsection{Usage}
All of the API endpoints used by our backend is the same as how we give data for our customer. Our  customer will be able to utilize our API by an appropriate HTTP request:
\begin{enumerate}
  \item API usage page including pagination parameters:

https://api.congressexposed.me or https://api.congressexposed.me/api

  \item Get Issues:

https://api.congressexposed.me/api/issue
  \item Get Specific Issues (issue-id = 8166):

https://api.congressexposed.me/api/issue/8166
  \item Get Bill:

https://api.congressexposed.me/api/bill
  \item Get Specific BIll (bill-id = hconres1-115):

https://api.congressexposed.me/api/bill/hconres1-115
  \item Get Committee:

https://api.congressexposed.me/api/committee
  \item Get Specific Committee (committee-id = HSAG):

https://api.congressexposed.me/api/committee
  \item Get Member:

https://api.congressexposed.me/api/member
  \item Get Specific Member (member-id = A000360):

https://api.congressexposed.me/api/member/A000360
\end{enumerate}

The Postman Documentation will clarify most of the question :

https://documenter.getpostman.com/view/6751446/S11LrHWE
\subsection{Notes}
In the current phase, due to a large amount of data, the main model's unique ID will be the only query parameter for now. In the future phase, we will discuss with our frontend and customer to decide on what query parameters are most demanded, then we will implement searching, filtering, and sorting to our API endpoints.

\section{Models}
We have four models for our website as well as an \textit{About} page.

\subsection{Congresspeople}
The first model is the senators and representatives in Congress (Congresspeople). For each instance, we have the following attributes:
\begin{enumerate}
  \item Name
  \item Party
  \item Picture
  \item Facebook account
  \item Twitter Account
  \item Biography
  \item Recent votes
\end{enumerate}

We display each congressperson's name, picture, Twitter account, and Facebook account in the model page in each card. On the instance page, we display the remaining attributes. For the Facebook and Twitter accounts, we additionally embed the Twitter timeline and Facebook page into the model page.

\subsection{Bills}
The second model is bills in consideration by Congress. For each bill, we have the following attributes:
\begin{enumerate}
  \item ID
  \item Short title
  \item Full title
  \item Sponsoring member
  \item Summary of the bill
  \item Date when it passed the House, or \texttt{null} if it did not.
  \item Date when it passed the Senate, or \texttt{null} if it did not.
  \item Date when it was enacted into law, or \texttt{null} if it did not.
  \item Date when it was vetoed, or \texttt{null} if it did not.
\end{enumerate}
We display the bill ID, either the short or full title (preferring the short title if it exists), and the dates of when it was passed, enacted, and/or vetoed in each card in the model page. The remaining attributes are displayed on the instance page.


\subsection{Committees}
The third model is on Committees, which are groups of persons elected or appointed to provide some service or function. For each committee, we have the following attributes:
\begin{enumerate}
  \item Chair Party
  \item Committee ID
  \item Congress
  \item Name
  \item Website
  \item Committee members
  \item Related Subcommittees
\end{enumerate}
Each Committee member listed on the Committee instance page is an associated congressperson, and it is linked to that congressperson's instance page. All attributes except
the Committee members and Related Subcommittees are displayed on the model page, while all of them are displayed on the corresponding committee instance page

\subsection{Issue/Category}
The fourth model is on the various category of contemporary issues that need to be addressed. For each issue/category, we have the following attributes:
\begin{enumerate}
  \item Name
  \item Picture
  \item Associated bills
\end{enumerate}

The first two attributes can be seen on the cards on the model page. The associated bills attribute links to specific bills that address this issue and can be seen on the instance page.

\subsection{About}
The About page provided a short description of our website as well as our motivation in creating this site. We also incorporated some information on each member of our team, such as a short biography, the number of commits, issues, etc. Lastly, we included the tools we have used for this project along with links to our GitLab repository and Postman.

\section{Tools}

\subsection{React}
We used React to structure our website. React makes it easy to build reusable components and compose them into websites. We integrated React with a very useful third-party library called React Router to give our web application the ability to have multiple pages and links between pages. Each model is displayed on a separate page, and we have links on each model page to view specific model instances. Each page also has a shared navigation bar which contains links to all model pages as well as an About page.

One of the design patterns that React promotes is component-based design. The idea is to build reusable components that can be referenced from multiple parts of the application. One of the components we created is the \texttt{RenderCards} component in \texttt{frontend/src/Components/RenderCards.js}. This component lays out ``cards'' for each instance in a grid. Before adding this component, we had manually constructed grids and cards for each instance. This component allowed us to factor out the common logic and simplified a lot of our code.

\subsection{Namecheap}
Namecheap was our domain name registrar. Although Namecheap provides a DNS service, we chose to reroute DNS to Amazon's Route53 instead, as described in section 6.

\subsection{Postman}
In phase II, we utilized Postman as a mean to design the API endpoints for our REST API and as a mean to test our API endpoints.

\subsection{Bootstrap}
Bootstrap is the HTML and CSS framework we used to style our webpages. Bootstrap contains several components such as a navigation bar (\texttt{navbar}) and has a CSS grid layout system to place content on webpages. The CSS grid layout contains rows and columns which can be used to create responsive pages. Instead of using Bootstrap directly, we used the third-party Reactstrap library which provides React component wrappers around the Bootstrap components.

\subsection{AWS}
We used Amazon Web Services to host our web application. Specifically, the following services were utilized: AWS Elastic Beanstalk, Route53, Certificate Manager, CloudFront, and S3. For more details, please see section 6 below.

\subsection{GitLab}
We used GitLab for cross-team collaboration, issue tracking, and repository hosting. We used GitLab to track progress and assign team members to tasks.

\subsection{MySQL}
We used MySQL to store our data as described in Section~\ref{Section-Database}.

\subsection{Flask, Flask-SQLAlchemy, SQLAlchemy}
We used Flask for our backend server. Flask is a popular web framework for Python. We also used SQLAlchemy to connect our backend server to our MySQL database. SQLAlchemy creates Python classes that map to SQL tables which allows us to get information from the database easily. We used Flask-SQLAlchemy to link Flask and SQLAlchemy together.

\subsection{Mocha}
We used Mocha for unit testing our frontend code as described in Section~\ref{Section-Mocha}.

\subsection{Selenium}
We used Selenium for acceptance testing as described in Section~\ref{Section-Selenium}.

\section{Hosting}
As stated above, AWS was our hosting platform of choice. After securing our domain, we had to accomplish the following tasks: map our domain name to our web application, get a SSL certificate to route http to https,
and deploy our web application. To start with, we created an ElasticBeanstalk instance with a Docker container as our choice of deployment. We downloaded the AWS CLI to help streamline a majority of the AWS workload.
Next, we also create an S3 bucket and used it to host a static website. Once again, deployment was made easy with the use of the AWS CLI. If using the AWS CLI, you must get an Auth token(key), which can be done by
going to the AWS portal, and under your profile settings go to security settings to generate a key.

Both ElasticBeanstalk and S3 provide an endpoint URL for our deployed web applications. Our next step is to map our domain name to these endpoints. We begin by going to AWS Route53 and creating a hosted zone.
Next, we go to our DNS provider (in this case NameCheap), and add the NS records (name servers) to NameCheap. Next, we come back to Route53, and add CNAME Records (mapping domain names to domain names, as opposed to
A records, which map IP addresses to domain names), and to route our domain name requests to our web application's endpoint. Our next step is to secure an SSL certificate.

Before we get to the SSL certificate, we must introduce AWS CloudFront. We create a CF distribution to host our S3 static website. Once we have done this, we go back to
Route53 and create CNAME records to route our domain name requests to the CF distribution's endpoint. You must look into cache invalidations, as this is necessary to prevent
serving old files to users around the specified region.

Finally, we go to AWS Certificate Manager, and create a request for a new certificate. Once we obtain that certificate, we can choose to upload it in either S3
through CloudFront, or in the ElasticBeanstalk instance. To do it in the latter, we must create a LoadBalancer that redirects \texttt{HTTP} requests to https requests. In both cases,
we apply the SSL certificate natively in AWS and this allows us to redirect all traffic to https.

To maintain our web application, you must understand the basics of IP addresses, domain names, and CNAME records vs A records. You will need to delve into topics such as
cache invalidations in order to understand why content may not be updating at your edge locations through the CloudFront distribution. And finally, you will want to install and use the
AWS Command Line Interface as it streamlines the process of deploying your web application. In our case we had two different routes to achieve our goal for phase 1, the static website through
S3, or using a docker image and the PaaS service AWS ElasticBeanstalk.

\section{Pagination}

In this phase of the project, we were required to implement pagination for our model pages. Pagination accomplishes two things:
\begin{enumerate}
  \item Pagination on the API server-side reduces the API response size.
  \item Pagination on the frontend limits the amount of information shown to the user. Less data rendered means the page will load faster.
\end{enumerate}

For server-side pagination, we used the built-in pagination from the Flask-SQLAlchemy library. This automatically adds pages to the API response and requires the API consumer to specify a page number. For example, to get the 5\textsuperscript{th} page of bills, the API consumer would need to call the \texttt{/api/bill} endpoint with \texttt{page=5} URL parameter, so the URL would be \texttt{/api/bill?page=5}. The JSON response from Flask-SQLAlchemy provides the API consumer with the total number of pages, which we use in the frontend.

We used the \texttt{react-js-pagination} library for pagination in the frontend. This library provides a React component called \texttt{Pagination} that displays a bar showing page numbers and arrows for quickly jumping to the first or last page. We also created our own React component called \texttt{ModelPagination} (in \texttt{frontend/src/Components/ModelPagination.js}) that wraps the \texttt{Pagination} component and provides it with information like the current page number and the total number of pages. We also wrote JavaScript functions to get data from the API endpoints and these functions take a \texttt{pageNumber} argument, which is used to create the API endpoint URL described earlier that will be fetched.

\section{Database}\label{Section-Database}

We used MySQL for our database. We crawled data and stored the information we collected in MySQL, then we used SQLAlchemy and Flask to retrieve data for our backend server.

\subsection{Layout}
\begin{figure}[h!]
  \centering
  \includegraphics[width=0.9\textwidth]{Class Diagram With UML Notation.png}
\end{figure}

We have four models in our database:
\begin{enumerate}
  \item Member (Congressperson)
  \item Bill
  \item Committee
  \item Issue
\end{enumerate}
Each model has several attributes and is linked to other models as shown in the UML diagram. The models are connected as follows:
\begin{enumerate}
  \item Each \texttt{Committee} has several \texttt{Subcommittee}s that are linked by the \texttt{parent\_committee\_id} column in \texttt{Subcommittee}.
  \item Each \texttt{Bill} may have a sponsoring member (link to \texttt{Member}) and several cosponsoring members (link to \texttt{Member} through the association table \texttt{Bill\_Cosponsor}). Bills can also be sponsored by committees (link to \texttt{Committee} through the association table \texttt{Bill\_Committee}) and subcommittees (link to \texttt{Subcommittee} through the association table \texttt{Bill\_Subcommittee}). Members of Congress vote on bills, sometimes more than once or not at all, and these votes are also stored in the database. We use the \texttt{RollCall} table to specify individual ``vote sessions'', which are associated with \texttt{Bill} through the \texttt{bill\_id} column and are associated with \texttt{Member} through the \texttt{RollCallPosition} association table.
  \item The \texttt{Member} table is linked to the \texttt{Member\_Role} which describes each congressperson's roles in the House and Senate. \texttt{Member\_Role} is linked to \texttt{Committee} through an association table called \texttt{Member\_Role\_Committee}. Members of Congress can also be part of subcommittees, so \texttt{Member\_Role} links to \texttt{Subcommittee} through an association table called \texttt{Member\_Role\_Subcommittee}.
  \item Each \texttt{Issue} is connected to \texttt{Bill} through the \texttt{Issue\_Bill} association table. This makes it possible to get all the issues relevant to a particular bill, and all the bills that are part of a particular issue.
\end{enumerate}

\subsection{Adding data}
We hosted our MySQL instance on Amazon RDS. To add information to the database, we wrote a Python script (\texttt{crawler/crawler.py}) that crawls our data sources and outputs a SQL script containing \texttt{CREATE TABLE} and \texttt{INSERT INTO} statements. Then we log in to our database server and run our SQL script using the \texttt{source} command. This two-step process allows us to manually check the SQL script before adding data to our database. We chose to generate a raw SQL script instead of using an ORM like SQLAlchemy because the total number of rows in our database is around 900,000 and SQLAlchemy is not designed for inserting data in bulk.

\subsection{Retrieving data}
To retrieve data from our database in our backend server, we used SQLAlchemy. SQLAlchemy allows us to automatically load tables from the database as Python classes with attributes named after the columns in the table. Our Flask API code then uses these tables to return data, with the help of the Flask-SQLAlchemy library.

\section{Testing}
\subsection{Frontend Unit Testing} \label{Section-Mocha}
We used Mocha for our frontend unit testing. % TODO

\subsection{Frontend Acceptance Testing} \label{Section-Selenium}
We used Selenium for testing our frontend. Selenium is a browser automation tool that is used for automatically testing websites. The acceptance tests we wrote using Selenium test the website as it looks in the browser. These tests focus mainly on model pages and instance page links, pagination, and accessibility (such as including \texttt{alt} tags for all images).

\subsection{Backend Unit Testing}
For our Backend Unit Testing, we used two testing methods: Postman and the Python \texttt{unittest} module

Postman has a default testing tool and we checked that every API endpoint returns HTTP code 200. This also checks for response time under 200ms, but due to the large dataset some of the endpoints take longer to return data. We will add the response time test after further refining our backend code.

We used the Python \texttt{unittest} module to check our API. These unit tests also test the helper functions we implemented to connect models.


\end{document}
