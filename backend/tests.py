from unittest import main, TestCase
import requests
import json
import time

# url = "http://api.congressexposed.me/"
url = "http://127.0.0.1:5000/api/"
models = ["member", "committee", "issue", "bill"]


def request_retry(url):
    global sleeptime
    r = ""
    while r == "":
        try:
            r = requests.get(url)
        except requests.exceptions.RequestException:
            print("Retrying")
            time.sleep(10)
            continue

    return r


class TestSWE(TestCase):
    def testMember(self):
        model = models[0]
        print("testing " + model)
        r = request_retry(url + model)
        self.assertEqual(r.status_code, 200)
        d = json.loads(r.text)
        o = d["objects"][0]["attributes"]
        o2 = d["objects"][0]["relationships"]
        self.assertIsInstance(
            o["biography"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(
            o["date_of_birth"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(
            o["facebook_account"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(
            o["first_name"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(
            o["last_name"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(
            o["member_id"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(
            o["party"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(
            o["picture_url"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(
            o["twitter_account"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(
            o["website"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(o2["roll_call_positions"], list)
        self.assertIsInstance(o2["cosponsored_bills"], list)

    def testCommittee(self):
        model = models[1]
        print("testing " + model)
        r = request_retry(url + model)
        self.assertEqual(r.status_code, 200)
        d = json.loads(r.text)
        o = d["objects"][0]["attributes"]
        o2 = d["objects"][0]["relationships"]
        self.assertIsInstance(o["congress"], int)
        self.assertIsInstance(
            o["name"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(o["committee_id"], int)
        self.assertIsInstance(
            o["chair_party"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(
            o["website"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(o2["bills"], list)
        self.assertIsInstance(o2["member_roles"], list)
        self.assertIsInstance(o2["subcommittees"], list)

    def testIssue(self):
        model = models[2]
        print("testing " + model)
        r = request_retry(url + model)
        self.assertEqual(r.status_code, 200)
        d = json.loads(r.text)
        o = d["objects"][0]["attributes"]
        o2 = d["objects"][0]["relationships"]
        self.assertIsInstance(o["issue_id"], int)
        self.assertIsInstance(
            o["name"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(o2["bills"], list)

        self.assertIsInstance(
            o["picture_url"].encode("ascii", "ignore"), (str, type(None), bytes)
        )

    def testBill(self):
        model = models[3]
        print("testing " + model)
        r = request_retry(url + model)
        self.assertEqual(r.status_code, 200)
        d = json.loads(r.text)
        o = d["objects"][0]["attributes"]
        o2 = d["objects"][0]["relationships"]

        self.assertIsInstance(
            o["bill_id"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(o["congress"], int)
        self.assertIsInstance(o["enacted_date"], (str, type(None), bytes))
        self.assertIsInstance(
            o["full_title"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(
            o["short_title"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(
            o2["sponsored_by"].encode("ascii", "ignore"), (str, type(None), bytes)
        )
        self.assertIsInstance(o2["committees"], list)
        self.assertIsInstance(o2["rollcalls"], list)
        self.assertIsInstance(o2["subcommittees"], list)
        self.assertIsInstance(o2["cosponsors"], list)
        self.assertIsInstance(o2["issues"], list)

        self.assertIsInstance(
            o["house_passage_date"].encode("ascii", "ignore"), (type(None), bytes)
        )
        self.assertIsInstance(o["senate_passage_date"], (type(None), bytes))
        self.assertIsInstance(
            o["summary"].encode("ascii", "ignore"), (type(None), bytes)
        )

        self.assertIsInstance(o["vetoed_date"], (type(None), bytes))

    def testPage(self):
        print("testing page number")
        for i in models:
            r = request_retry(url + i + "?page=2")
            self.assertEqual(r.status_code, 200)

    def testPageResult(self):
        print("testing result per page")
        for i in models:
            r = request_retry(url + i + "?result_per_page=8")
            self.assertEqual(r.status_code, 200)

    def testPagination(self):
        print("testing pagination")
        for i in models:
            r = request_retry(url + i + "?result_per_page=8&page=2")
            self.assertEqual(r.status_code, 200)

    def testMain1(self):
        print("testing main page")
        r = request_retry("http://127.0.0.1:5000/api")
        self.assertEqual(r.status_code, 200)

    def testMain2(self):
        print("testing another main page")
        r = request_retry("http://127.0.0.1:5000")
        self.assertEqual(r.status_code, 200)

    def testIssueFilter1(self):
        print("testing Issue filter")
        r = request_retry("http://127.0.0.1:5000/api/issue/filter[objects]=[{}]")
        self.assertEqual(r.status_code, 200)

    def testIssueFilter2(self):
        print("testing Issue filter")
        r = request_retry(
            'http://127.0.0.1:5000/api/issue/filter[objects]=[{"name":"Air"}]'
        )
        self.assertEqual(r.status_code, 200)

    def testIssueFilter3(self):
        print("testing Issue filter")
        r = request_retry(
            'http://127.0.0.1:5000/api/issue/filter[objects]=[{"name":"Air","lobbying": "Randall E. Davis"}]'
        )
        self.assertEqual(r.status_code, 200)

    def testIssueFilter4(self):
        print("testing Issue filter")
        r = request_retry(
            'http://127.0.0.1:5000/api/issue/filter[objects]=[{"name":"Air","lobbying": "Randall E. Davis","issue_id": 8572}]'
        )
        self.assertEqual(r.status_code, 200)

    def testMemberFilter1(self):
        print("testing Member filter")
        r = request_retry(
            'http://127.0.0.1:5000/api/member/filter[objects]=[{"sort":"member_id","order":"asc"}]'
        )
        self.assertEqual(r.status_code, 200)

    def testMemberFilter2(self):
        print("testing Member filter")
        r = request_retry(
            'http://127.0.0.1:5000/api/member/filter[objects]=[{"member_id":"A"}]'
        )
        self.assertEqual(r.status_code, 200)

    def testMemberFilter3(self):
        print("testing Member filter")
        r = request_retry(
            'http://127.0.0.1:5000/api/member/filter[objects]=[{"first_name":"A","sort":"last_name","order":"desc"}]'
        )
        self.assertEqual(r.status_code, 200)

    def testMemberFilter4(self):
        print("testing Member filter")
        r = request_retry(
            'http://127.0.0.1:5000/api/member/filter[objects]=[{"last_name":"A","party": "R"}]'
        )
        self.assertEqual(r.status_code, 200)

    def testCommitteeFilter1(self):
        print("testing Committee filter")
        r = request_retry(
            'http://127.0.0.1:5000/api/committee/filter[objects]=[{"sort":"committee_code","order":"asc"}]'
        )
        self.assertEqual(r.status_code, 200)

    def testCommitteeFilter2(self):
        print("testing Committee filter")
        r = request_retry(
            'http://127.0.0.1:5000/api/committee/filter[objects]=[{"name":"A"}]'
        )
        self.assertEqual(r.status_code, 200)

    def testCommitteeFilter3(self):
        print("testing Committee filter")
        r = request_retry(
            'http://127.0.0.1:5000/api/committee/filter[objects]=[{"subcommittee_code":"A","sort":"name","order":"desc"}]'
        )
        self.assertEqual(r.status_code, 200)

    def testCommitteeFilter4(self):
        print("testing Committee filter")
        r = request_retry(
            'http://127.0.0.1:5000/api/committee/filter[objects]=[{"subcommittee_code":"A","committee_code": "A"}]'
        )
        self.assertEqual(r.status_code, 200)

    def testBillFilter1(self):
        print("testing Bill filter")
        r = request_retry(
            'http://127.0.0.1:5000/api/bill/filter[objects]=[{"sort":"enacted_date","order":"asc"}]'
        )
        self.assertEqual(r.status_code, 200)

    def testBillFilter2(self):
        print("testing Bill filter")
        r = request_retry(
            'http://127.0.0.1:5000/api/bill/filter[objects]=[{"full_title":"A"}]'
        )
        self.assertEqual(r.status_code, 200)

    def testBillFilter3(self):
        print("testing Bill filter")
        r = request_retry(
            'http://127.0.0.1:5000/api/bill/filter[objects]=[{"short_title":"A","sort":"house_passage_date","order":"desc"}]'
        )
        self.assertEqual(r.status_code, 200)

    def testBillFilter4(self):
        print("testing Bill filter")
        r = request_retry(
            'http://127.0.0.1:5000/api/bill/filter[objects]=[{"bill_id":"A","short_title": "A"}]'
        )
        self.assertEqual(r.status_code, 200)

    def testVisualize1(self):
        print("testing visualize 1")
        r = request_retry("http://127.0.0.1:5000/api/visualize_1")
        self.assertEqual(r.status_code, 200)

    def testVisualize2(self):
        print("testing visualize 2")
        r = request_retry("http://127.0.0.1:5000/api/visualize_2")
        self.assertEqual(r.status_code, 200)

    def testVisualize3(self):
        print("testing visualize 3")
        r = request_retry("http://127.0.0.1:5000/api/visualize_3")
        self.assertEqual(r.status_code, 200)


if __name__ == "__main__":
    main()
