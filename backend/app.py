import flask
from flask_sqlalchemy import SQLAlchemy
import json
from flask import jsonify, request
from flask_cors import CORS
from sqlalchemy import and_
from sqlalchemy import or_, PrimaryKeyConstraint, Column, Integer, String
from sqlalchemy.orm.session import sessionmaker
from model import (
    Bill,
    Committee,
    Issue,
    IssueNumBills,
    Member,
    MemberRole,
    MemberRoleCommittee,
    MemberRoleSubcommittee,
    RollCall,
    RollCallPosition,
    Subcommittee,
)

MAX = 10000

application = flask.Flask(__name__)
# allow cross domains
CORS(application)

application.config["DEBUG"] = False
application.config[
    "SQLALCHEMY_DATABASE_URI"
] = "mysql://master:masterpassword@mydb.ceoq3fxzofj3.us-east-1.rds.amazonaws.com/testdb"
application.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(application)
db.Model.metadata.reflect(db.engine)

Session = sessionmaker()
session = Session()


# Helper methods
def full_name(first_name, middle_name, last_name):
    if middle_name is None or middle_name == "":
        return "%s %s" % (first_name, last_name)
    return "%s %s %s" % (first_name, middle_name, last_name)


def get_links():
    links = request.args.get("links")
    if links is None or links != "False":
        return True
    return False


def get_page(page):
    ret = dict()
    ret["num_results"] = page.total
    bill = page.items
    obj = []
    for i in bill:
        obj.append(i.serialize(links=get_links()))

    ret["objects"] = obj
    ret["page"] = page.page
    ret["total_pages"] = page.pages
    return ret


def pagination():
    per_page = request.args.get("result_per_page")
    page_num = request.args.get("page")
    if per_page is None:
        per_page = 9
    if page_num is None:
        page_num = 1
    return per_page, page_num


# General API endpoints


@application.route("/")
@application.route("/api")
def congress_expose():
    intro = list()
    intro.append(
        """In order to view the api page, google chrome extension: jsonview is highly recommended:
                https://chrome.google.com/webstore/detail/jsonview/chklaanhfefbnpoihckbnefhakgolnmc?hl=en"""
    )
    intro.append("""Here is a list of urls for four main model:""")
    intro.append("""    List of Bills: api.congressexposed.me/api/bill""")
    intro.append("""    Specific Bill: api.congressexposed.me/api/bill/{bill-id}""")
    intro.append("""    List of Issues: api.congressexposed.me/api/issue""")
    intro.append("""    Specific Issue: api.congressexposed.me/api/issue/{issue-id}""")
    intro.append("""    List of Committees: api.congressexposed.me/api/committee""")
    intro.append(
        """    Specific Committee: api.congressexposed.me/api/committee/{committee-id}"""
    )
    intro.append("""    List of Congress People: api.congressexposed.me/api/member""")
    intro.append(
        """    Specific Congress People: api.congressexposed.me/api/member/{member-id}"""
    )
    intro.append("""When requesting a list of objects, pagination is present""")
    intro.append(
        """The default result per page is 9, to get more or less result_per_page:
    Ex: api.congressexposed.me/api/issue?result_per_page=3"""
    )
    intro.append(
        """The default page is the first page, to get specific page:
        Ex: api.congressexposed.me/api/issue?page=3"""
    )
    return jsonify(intro)


@application.route("/api/bill")
def get_bill():
    per_page, page_num = pagination()
    page = Bill.query.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/issue")
def get_issue():
    per_page, page_num = pagination()
    page = Issue.query.order_by(Issue.issue_id).paginate(
        per_page=int(per_page), page=int(page_num)
    )
    return jsonify(get_page(page))


@application.route("/api/member")
def get_member():
    per_page, page_num = pagination()
    page = Member.query.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/committee")
def get_committee():
    per_page, page_num = pagination()
    page = Committee.query.filter_by(congress=116).paginate(
        per_page=int(per_page), page=int(page_num)
    )
    return jsonify(get_page(page))


@application.route("/api/subcommittee")
def get_subcommittee():
    per_page, page_num = pagination()
    page = Subcommittee.query.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/roll_call_position")
def get_roll_call_position():
    per_page, page_num = pagination()
    page = RollCallPosition.query.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/roll_call")
def get_roll_call():
    per_page, page_num = pagination()
    page = RollCall.query.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/member_role")
def get_member_role():
    per_page, page_num = pagination()
    page = MemberRole.query.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/member_role_committee")
def get_member_role_committee():
    per_page, page_num = pagination()
    page = MemberRoleCommittee.query.paginate(
        per_page=int(per_page), page=int(page_num)
    )
    return jsonify(get_page(page))


@application.route("/api/member_role_subcommittee")
def get_member_role_subcommittee():
    per_page, page_num = pagination()
    page = MemberRoleSubcommittee.query.paginate(
        per_page=int(per_page), page=int(page_num)
    )
    return jsonify(get_page(page))


# Specific object endpoint


@application.route("/api/issue/<issue_id>")
def get_specific_issue(issue_id):
    issue = Issue.query.filter_by(issue_id=issue_id).first_or_404()
    return json.dumps(
        issue.serialize(links=get_links()), ensure_ascii=False, indent=None
    )


@application.route("/api/rollcallposition/<rollcallposition_id>")
def get_specific_rollcallposition(rollcallposition_id):
    rcp = RollCallPosition.query.filter_by(
        rollcallposition_id=rollcallposition_id
    ).first_or_404()
    return json.dumps(rcp.serialize(links=get_links()), ensure_ascii=False, indent=None)


@application.route("/api/bill/<bill_id>")
def get_specific_bill(bill_id):
    bill = Bill.query.filter_by(bill_id=bill_id).first_or_404()
    return json.dumps(
        bill.serialize(links=get_links()), ensure_ascii=False, indent=None
    )


@application.route("/api/member/<member_id>")
def get_specific_member(member_id):
    member = Member.query.filter_by(member_id=member_id).first_or_404()
    return json.dumps(
        member.serialize(links=get_links()), ensure_ascii=False, indent=None
    )


@application.route("/api/committee/<committee_id>")
def get_specific_committees(committee_id):
    com = Committee.query.filter_by(committee_id=committee_id).first_or_404()
    return json.dumps(com.serialize(links=get_links()), ensure_ascii=False, indent=None)


@application.route("/api/subcommittee/<subcommittee_id>")
def get_specific_subcommittees(subcommittee_id):
    subcom = Subcommittee.query.filter_by(
        subcommittee_id=subcommittee_id
    ).first_or_404()
    return json.dumps(
        subcom.serialize(links=get_links()), ensure_ascii=False, indent=None
    )


@application.route("/api/member_role/<member_role_id>")
def get_specific_member_role(member_role_id):
    mem = MemberRole.query.filter(
        MemberRole.member_role_id == member_role_id
    ).first_or_404()
    return json.dumps(mem.serialize(links=get_links()), ensure_ascii=False, indent=None)


@application.route("/api/member_role_committee/<member_role_committee_id>")
def get_specific_member_role_committee(member_role_committee_id):
    mem = MemberRoleCommittee.query.filter(
        MemberRoleCommittee.member_role_committee_id == member_role_committee_id
    ).first_or_404()
    return json.dumps(mem.serialize(links=get_links()), ensure_ascii=False, indent=None)


@application.route("/api/member_role_subcommittee/<member_role_subcommittee_id>")
def get_specific_member_role_subcommittee(member_role_subcommittee_id):
    mem = MemberRoleSubcommittee.query.filter(
        MemberRoleSubcommittee.member_role_subcommittee_id
        == member_role_subcommittee_id
    ).first_or_404()
    return json.dumps(mem.serialize(links=get_links()), ensure_ascii=False, indent=None)


# filtering, sorting, searching


# Example Call: /api/issues/filter[objects]=[{"name":"Air"}]
def googleLike(text_search_keys, name, dicObj):
    text_searches = []
    for key in text_search_keys:
        if key in dicObj:
            words = dicObj[key].split(" ")
            attribute = getattr(name, key)
            for w in words:
                text_searches.append(attribute.ilike("%" + w + "%"))
    return text_searches


def sorthelper(sorter, name, a, dicObj):
    if dicObj["order"] == "desc":
        for x in sorter:
            if dicObj["sort"] == x:
                attribute = getattr(name, x)
                a = a.order_by(attribute.desc())
                break
    else:
        for x in sorter:
            if dicObj["sort"] == x:
                attribute = getattr(name, x)
                a = a.order_by(attribute)
                break
    return a


@application.route("/api/issue/filter[objects]=[<objectDic>]")
def get_all_issue_filter_name(**objectDic):
    per_page, page_num = pagination()
    dicObj = json.loads(objectDic["objectDic"])

    range_queries = []
    if "num_bills" in dicObj:
        range_queries.append(IssueNumBills.num_bills >= int(dicObj["num_bills"]["min"]))
        range_queries.append(IssueNumBills.num_bills <= int(dicObj["num_bills"]["max"]))

    if "num_bills_passed" in dicObj:
        range_queries.append(
            IssueNumBills.num_bills_passed >= int(dicObj["num_bills_passed"]["min"])
        )
        range_queries.append(
            IssueNumBills.num_bills_passed <= int(dicObj["num_bills_passed"]["max"])
        )

    text_searches = googleLike(["name", "lobbying"], IssueNumBills, dicObj)
    a = IssueNumBills.query.filter(and_(or_(*text_searches), *range_queries))
    if "sort" in dicObj:
        a = sorthelper(
            ["name", "lobbying", "issue_id", "num_bills", "num_bills_passed"],
            IssueNumBills,
            a,
            dicObj,
        )

    page = a.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/member/filter[objects]=[<objectDic>]")
def get_all_member_filter_name(**objectDic):
    per_page, page_num = pagination()
    dicObj = json.loads(objectDic["objectDic"])

    text_searches = googleLike(
        [
            "member_id",
            "first_name",
            "last_name",
            "middle_name",
            "twitter_account",
            "facebook_account",
            "biography",
        ],
        Member,
        dicObj,
    )

    filters = []
    if "party" in dicObj:
        if dicObj["party"] is None:
            filters.append(Member.party is None)
        else:
            filters.append(Member.party.ilike("%" + str(dicObj["party"]) + "%"))

    a = Member.query.filter(and_(or_(*text_searches), *filters))
    if "sort" in dicObj:
        a = sorthelper(
            ["first_name", "last_name", "middle_name", "member_id"], Member, a, dicObj
        )
    page = a.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/committee/filter[objects]=[<objectDic>]")
def get_all_committee_filter_name(**objectDic):
    per_page, page_num = pagination()
    dicObj = json.loads(objectDic["objectDic"])

    filters = []
    if "congress" in dicObj:
        c = int(dicObj["congress"])
        filters.append(Committee.congress == c)

    if "chair_party" in dicObj:
        if dicObj["chair_party"] is None:
            filters.append(
                Committee.chair_party is None
            )  # NOTE: we cannot use `Committee.chair_party is None`
        else:
            filters.append(
                Committee.chair_party.ilike("%" + str(dicObj["chair_party"]) + "%")
            )
    text_searches = googleLike(["committee_code", "name", "website"], Committee, dicObj)
    a = db.session.query(Committee).filter(and_(or_(*text_searches), *filters))

    if "sort" in dicObj:
        if dicObj["order"] == "desc":
            a = a.order_by(getattr(Committee, dicObj["sort"]).desc())
        else:
            a = a.order_by(getattr(Committee, dicObj["sort"]))

    page = a.paginate(per_page=int(per_page), page=int(page_num))

    return jsonify(get_page(page))


@application.route("/api/bill/filter[objects]=[<objectDic>]")
def get_all_bill_filter_name(**objectDic):
    per_page, page_num = pagination()
    dicObj = json.loads(objectDic["objectDic"])

    if "congress" in dicObj:
        j = "%" + str(dicObj["congress"]) + "%"
    else:
        j = "%%%"
    text_searches = googleLike(
        ["summary", "bill_id", "full_title", "short_title", "sponsoring_member_id"],
        Bill,
        dicObj,
    )

    a = Bill.query.filter(and_(or_(*text_searches), Bill.congress.ilike(j)))
    if "sort" in dicObj:
        a = sorthelper(
            [
                "bill_id",
                "sponsoring_member_id",
                "congress",
                "senate_passage_date",
                "enacted_date",
                "vetoed_date",
                "house_passage_date",
            ],
            Bill,
            a,
            dicObj,
        )

    page = a.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


# endpoint for visualization
@application.route("/api/visualize_1")
def visualize_1():
    page = Committee.query.filter_by(congress=116).paginate(per_page=MAX)
    ret = dict()
    for i in page.items:
        i = i.serialize(links=True)
        ret[i["attributes"]["name"]] = len(i["relationships"]["members"])
    return jsonify(ret)


@application.route("/api/visualize_2")
def visualize_2():
    page = IssueNumBills.query.filter(IssueNumBills.num_bills > 500).paginate(
        per_page=MAX
    )
    ret = dict()
    for i in page.items:
        i = i.serialize(links=False)
        ret[i["attributes"]["name"]] = (
            float(i["attributes"]["num_bills_passed"])
            / i["attributes"]["num_bills"]
            * 100
        )

    return jsonify(ret)


def v3helper(a, d):
    for k in d:
        s = dict()
        s["x"] = k
        s["y"] = d[k]
        a.append(s)


def createReturnData(none, one, both, total):
    ret = []
    a1 = []
    a2 = []
    a3 = []
    a4 = []
    v3helper(a1, none)
    v3helper(a2, one)
    v3helper(a3, both)
    v3helper(a4, total)
    group1 = dict()
    group2 = dict()
    group3 = dict()
    group4 = dict()
    group1["None"] = a1
    group2["Single"] = a2
    group3["Both"] = a3
    group3["Total"] = a4
    ret.append(group1)
    ret.append(group2)
    ret.append(group3)
    ret.append(group4)
    return ret


@application.route("/api/visualize_3")
def visualize_3():
    page = Member.query.paginate(per_page=MAX)
    total = dict()
    none = dict()
    both = dict()
    one = dict()
    for i in page.items:
        i = i.serialize(links=False)["attributes"]
        birthdate = i["date_of_birth"]
        age = 2019 - int(birthdate[0:4])
        if i["facebook_account"] and i["twitter_account"]:
            if age not in both:
                both[age] = 0
            both[age] += 1

        elif i["facebook_account"] or i["twitter_account"]:
            if age not in one:
                one[age] = 0
            one[age] += 1

        else:
            if age not in none:
                none[age] = 0
            none[age] += 1

        if age not in total:
            total[age] = 0
        total[age] += 1

    return jsonify(createReturnData(none, one, both, total))


if __name__ == "__main__":
    application.run()
