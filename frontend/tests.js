import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Container, Nav, NavItem, NavLink, Navbar, NavbarBrand, NavbarToggler } from 'reactstrap';
import Enzyme from 'enzyme';
import {describe, it} from 'mocha';
import {expect} from 'chai';
import {shallow, mount, render} from 'enzyme';

import About from './src/Pages/About/About.js';
import Home from './src/Pages/Home/Home.js';
import App from './src/App.js';
import PartyLabel from './src/Components/PartyLabel.js';
import TextHighlight from './src/Components/TextHighlight.js';
import Search from './src/Components/Search.js';

import makeUserFriendlyLabel from './src/Misc/makeUserFriendlyLabel.js';

import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });
const assert = require('chai').assert;

// General Page
describe('General Page', function() {
   const wrapper = shallow(<App/>);
   it('should render', function() {
     expect(wrapper.length).to.equal(1);
   });
   it('should have navigation bar', function() {
     expect(wrapper.find('NavItem').length).to.equal(8);
   });
   it('should have link to Committees', function() {
     expect(wrapper.find('Route[path="/Committees"]').length).to.equal(1);
   });
   it('should have link to Bills', function() {
     expect(wrapper.find('Route[path="/Bills"]').length).to.equal(1);
   });
   it('should have link to Issue_Category', function() {
     expect(wrapper.find('Route[path="/Issue_Category"]').length).to.equal(1);
   });
   it('should have link to Congresspeople', function() {
     expect(wrapper.find('Route[path="/Congresspeople"]').length).to.equal(1);
   });
   it('should have link to Home', function() {
     expect(wrapper.find('Route[path="/Home"]').length).to.equal(1);
   });
   it('should have link to visualization', function() {
     expect(wrapper.find('Route[path="/Visualization"]').length).to.equal(0);
   });
});

// About Page
describe('About Page', () => {
  const wrapper=render(<About />);
  // Rendering okay
  it('should render', () => {
    expect(wrapper.length).to.equal(1);
  });
  // Five members
  it('should have team member 1', () => {
    expect(wrapper.find('img[alt="Bohan Zhang"]').length).to.equal(1);
  });
  it('should have team member 2', () => {
    expect(wrapper.find('img[alt="David Durairaj"]').length).to.equal(1);
  });
  it('should have team member 3', () => {
    expect(wrapper.find('img[alt="Dilan Galindo Reyna"]').length).to.equal(1);
  });
  it('should have team member 4', () => {
    expect(wrapper.find('img[alt="Souvik Banerjee"]').length).to.equal(1);
  });
  it('should have team member 5', () => {
    expect(wrapper.find('img[alt="Yuki Kameyama"]').length).to.equal(1);
  });
});

// Home Page
describe('Home Page', () => {
  const wrapper=shallow(<Home/>);
  // Rendering okay
  it('should render', () => {
    expect(wrapper.length).to.equal(1);
  });
  // Has Title
  it('should have title', () => {
    expect(wrapper.find('div').length).to.equal(4);
  });
  // Carousel data
  it('should have 5 photos for Carousel', () => {
    expect(wrapper.find('img[alt="First Slide"]').length).to.equal(1);
    expect(wrapper.find('img[alt="Second Slide"]').length).to.equal(1);
    expect(wrapper.find('img[alt="Third Slide"]').length).to.equal(1);
    expect(wrapper.find('img[alt="Fourth Slide"]').length).to.equal(1);
    expect(wrapper.find('img[alt="Fifth Slide"]').length).to.equal(1);
  });
  // Logo
  it('should display the Congress logo', () => {
    expect(wrapper.find('img[alt="logo"]').length).to.equal(1);
  });
});

// Party Label
describe('Party Label', function() {
  const wrapper=shallow(PartyLabel({"party" : "republican"}));
  it('should run correctly', function() {
    expect(wrapper.length).to.equal(1);
  });
  it('should return correct color', function() {
    expect(wrapper.find('font[color="red"]').length).to.equal(1);
  });
});

// TextHighlight
describe("Text Highlight", function () {
  const wrapper = render((<TextHighlight text="software engineering" filterText={["software"]} />));
  it("should render", function () {
    expect(wrapper.length).to.equal(1);
  });
  it("should highlight something", function () {
    expect(wrapper.find("mark").length).to.equal(1);
  });
  it("should highlight software", function () {
    expect(wrapper.find("mark")[0].children[0].data).to.equal("software");
  });
});

describe("Text Highlight 2", function () {
  const wrapper = render((<TextHighlight text="Air" filterText={["Air"]} />));
  it("should render", function () {
    expect(wrapper.length).to.equal(1);
  });
  it("should highlight something", function () {
    expect(wrapper.find("mark").length).to.equal(1);
  });
});

describe("makeUserFriendlyLabel", function () {
  it("should make a user friendly label from first_name", function () {
    expect(makeUserFriendlyLabel("first_name")).to.equal("First Name");
  });
  it("should make a user friendly label from last_name", function () {
    expect(makeUserFriendlyLabel("last_name")).to.equal("Last Name");
  });
  it("should make a user friendly label from member_id", function () {
    expect(makeUserFriendlyLabel("member_id")).to.equal("Member ID");
  });
});

describe("Search", function () {
  const wrapper = render(<Search initialValue="search initial value"/>);
  it("should render", function () {
    expect(wrapper.length).to.equal(1);
  });

  it("should have an input box", function () {
    expect(wrapper.find("input").length).to.equal(1);
  });

  it("should have a search button", function() {
    expect(wrapper.find("button").length).to.equal(1);
  });

  it("should have placeholder text for input", function () {
    expect(wrapper.find("input")[0].attribs["placeholder"]).to.not.be.null;
  });
});
