import React, { Component } from 'react';
import styled from 'styled-components/macro';
import { Card, CardText, Button, CardBody, CardFooter, CardTitle, CardLink, CardSubtitle, Col, Row, Container } from 'reactstrap';
import { ProjectsBundle as GitLabProjects } from 'gitlab';
import RenderCards from '../../Components/RenderCards.js';
import BohanProfile from "./Bohan.png";
import DavidProfile from "./David.png";
import DilanProfile from "./Dilan.png";
import SouvikProfile from "./Souvik.png";
import YukiProfile from "./Yuki.png";
import Mocha from "./mocha.png";
import Background from "../../Components/background.png";

//psql "host=congressexposeddb.ceoq3fxzofj3.us-east-1.rds.amazonaws.com port=5432 sslmode=verify-full sslrootcert=/Users/dilangalindoreyna/Desktop/rds-combined-ca-bundle.pem dbname=congressexposeddb user=master"

const motivationText = `
Even though our government has been around for hundreds of years, there are
still quite a lot of obscurities, especially surrounding Congress, that
may be unfamiliar to the public eye. Therefore, our motivation is to
unveil the curtain that lies between the Congress and the people, and in
doing so, educate the general public on the inner workings of our government
along with some contemporary issues. Furthermore, studies have shown that
most Americans are uneducated when it comes to the workings of Congress,
and this is especially true concerning both people and bills. As a result,
we wish to enlighten people on any obscure issues that Congress may have
overlooked, blundered, or otherwise failed to provide an effective solution.`;

const whatWeDoText = `
Even though voter turn out rates have shown some increase over the years,
there are still a lot of mysteries surrounding Congress and our national
government in general. Therefore, the purpose of this site is to provide
some detailed information on the people who are currently running the nation,
with specific emphasis on the Legislative branch. We will be providing data
on various Congress people from both the House of Representatives and the Senate,
what is more, we will also be giving detailed examples on Bills, Committees,
and different categories of issues. What is more, our central theme will be
based on some seemingly controversial issues that Congress either struggled with
or had trouble coming up with effective solutions to.
`;

const CardImg = styled.img`
  max-width: 100%;
  min-height: 55%;
  max-height: 55%;
  width: 100%;
  height: 50%;
  object-fit: cover;
`;

var sectionStyle = {
  width: "auto",
  height: "auto",
  backgroundImage: `url(${Background})`,
  backgroundRepeat: "repeat"
};


const TeamMember = function (props) {
  let stats = null;
  if (props.numberOfCommits !== null && props.issuesOpened !== null && props.issuesClosed !== null && props.unitTests !== null) {
    stats = (<ul>
             <li>Number of commits: {props.numberOfCommits}</li>
             <li>Issues opened: {props.issuesOpened}</li>
             <li>Issues closed: {props.issuesClosed}</li>
             <li>Unit Tests: {props.unitTests}</li>
             </ul>);
  }
  return (
    <Card className="h-100 w-100">
      <CardImg alt={props.name} src={props.img} />
      <CardBody>
        <CardTitle><h3>{props.name}</h3></CardTitle>
        <CardSubtitle>{props.title}</CardSubtitle>
        <CardText>
          {props.bio}
        </CardText>
        {stats}
      </CardBody>
    </Card>
  );
};

const Data = function (props) {
  return (
    <Card className="h-100 w-100">
      <CardBody>
        <CardTitle className="text-center"><h3>{props.name}</h3></CardTitle>
        <CardText className = "text-center"><a href={props.url}>{props.url}</a></CardText>
      </CardBody>
    </Card>
  );
};

const Tool = function (props) {
  return (
    <Card className="h-100 w-100">
      <CardImg alt={props.name} src={props.img} />
      <CardBody>
        <CardTitle className="text-center"><h3>{props.name}</h3></CardTitle>
        <CardText className="text-center">{props.description}</CardText>
        <CardFooter className="text-center"><Button color="light"><CardLink href={props.url}>{props.url}</CardLink></Button></CardFooter>
      </CardBody>
    </Card>
  );
};

const ProjectLink = function (props) {
  return (
    <Card className="h-100 w-100">
      <CardBody>
        <CardTitle className="text-center"><h3>{props.name}</h3></CardTitle>
        <CardText className = "text-center"><a href={props.url}>{props.url}</a></CardText>
      </CardBody>
    </Card>
  );
};

const dataSources = [
  {"name": "CongressPeople",
   "url": "https://projects.propublica.org/api-docs/congress-api/members"},
  {"name": "Bills",
   "url": "https://projects.propublica.org/api-docs/congress-api/bills"},
  {"name": "Committees",
   "url": "https://www.cato.org/resources/data"},
  {"name": "Issues/Category",
   "url": "https://projects.propublica.org/api-docs/congress-api/bills/#get-a-specific-bill-subject"}
];

const toolsData = [
  {"name": "AWS",
   "url": "https://aws.amazon.com",
   "img": "https://automatedinsights.com/wp-content/uploads/2018/05/logos-square-aws.jpg",
   "description": "AWS was used to host this website."},
  {"name": "React",
   "url": "https://reactjs.org",
   "img": "https://cdn-images-1.medium.com/max/1600/1*XXF26vmDRr6vRY84d1BCKA.png",
   "description": "React was used to build components and pages."},
  {"name": "Bootstrap",
   "url": "https://getbootstrap.com",
   "img": "https://i1.wp.com/www.powderalliance.com/wp-content/uploads/bootstrap-logo.jpg?ssl=1",
   "description": "Bootstrap was used to style webpages."},
  {"name": "Postman",
   "url": "https://www.getpostman.com",
   "img": "https://images.g2crowd.com/uploads/product/image/large_detail/large_detail_1535488314/postman.jpg",
   "description": "Postman was used to design API endpoints."},
  {"name": "GitLab",
   "url": "https://gitlab.com",
   "img": "https://www.connectinternetsolutions.com/wp-content/uploads/2016/08/gitlab-e1471529374669-571x500.png",
   "description": "GitLab was used to host our code repository and for issue tracking."},
  {"name": "Namecheap",
   "url": "https://www.namecheap.com",
   "img": "https://media.apps.namecheap.com/tx_wlFgt_E80hfpK9Q4ZStmC.png",
   "description": "Namecheap was used to purchase our domain."},
  {"name": "Selenium",
   "img": "https://www.seleniumhq.org/images/big-logo.png",
   "url": "https://www.seleniumhq.org",
   "description": "Selenium was used for frontend acceptance tests."},
  {"name": "Flask",
   "url": "http://flask.pocoo.org",
   "img": "https://mherman.org/presentations/flask-kubernetes/images/flask-logo.png",
   "description": "Flask is the web framework we used for our backend."},
  {"name": "MySQL",
   "url": "https://www.mysql.com",
   "img": "https://pngimg.com/uploads/mysql/mysql_PNG35.png",
   "description": "We used MySQL for our database."},
  {"name": "SQLAlchemy",
   "url": "https://www.sqlalchemy.org",
   "img": "https://pbs.twimg.com/profile_images/476392134489014273/q5uAkmy7_400x400.png",
   "description": "SQLAlchemy was used to access the database from the backend."},
  {"name": "Docker",
   "url": "https://www.docker.com",
   "img": "https://www.linuxjournal.com/sites/default/files/styles/360_250/public/nodeimage/story/docker-logo.png?itok=3ep8JC02",
   "description": "Docker was used for deployment on AWS."},
  {"name": "Mocha",
   "url": "https://opencollective.com/mochajs",
   "img": Mocha,
   "description": "Mocha was used for Javascript testing."}
];

const linkData = [
  {"name": "GitLab repo",
   "url": "https://gitlab.com/DilanGalindo/swe"},
  {"name": "Postman",
   "url": "https://documenter.getpostman.com/view/6751446/S11LrHWE"}
];

// Main class
export default class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      commits: null,
      issues: null,
    };
    const projectID = 10905621;


    const service = new GitLabProjects({});
    Promise.all([service.Commits.all(projectID, {}), service.Issues.all({projectId: projectID})]).then(([commitData, issueData]) => {
      this.setState({ commits: commitData, issues: issueData });
    }).catch((err) => {
      console.log("Error: ", err);
    });
  }

  render() {

    const teamData = {
      "bohan": {"name": "Bohan Zhang",
                "title": "Frontend",
                "bio": "I m a sophomore Computer Science major who believes NASA is a group of aliens stuck on Earth and is trying to figure out how to reach home with our primitive technologies.",
                "img": BohanProfile,
                "numberOfCommits": null,
                "issuesOpened": null,
                "issuesClosed": null,
                "unitTests": 12},
      "david": {"name": "David Durairaj",
                "title": "Backend",
                "bio": "I'm a junior Computer Science major @UT Austin who likes to fly drones and play the acoustic guitar.",
                "img": DavidProfile,
                "numberOfCommits": null,
                "issuesOpened": null,
                "issuesClosed": null,
                "unitTests": 10},
      "dilan": {"name": "Dilan Galindo Reyna",
                "title": "Backend",
                "bio": "I'm Dilan. I am junior studying Computer Science as an undergraduate degree in the University of Texas at Austin.",
                "img": DilanProfile,
                "numberOfCommits": null,
                "issuesOpened": null,
                "issuesClosed": null,
                "unitTests": 20/* unittest */},
      "souvik": {"name": "Souvik Banerjee",
                 "title": "Frontend/Backend",
                 "bio": "I am a senior computer science and math major who likes food videos, working out, and public transit.",
                 "img": SouvikProfile,
                 "numberOfCommits": null,
                 "issuesOpened": null,
                 "issuesClosed": null,
                 "unitTests": 31 /* selenium */ + 11 /* mocha */},
      "yuki": {"name": "Yuki Kameyama",
               "title": "Backend",
               "bio": "Hi, I’m Yuki. I am a sophomore Computer science major, who likes playing videos game competitively.",
               "img": YukiProfile,
               "numberOfCommits": null,
               "issuesOpened": null,
               "issuesClosed": null,
               "unitTests": 59 /* postman */ + 8 /* unittest */}
    };

    let totalGitLabStats = null;
    if (this.state.commits !== null && this.state.issues !== null) {
      // API data has been downloaded

      // Set initial values to 0
      Object.values(teamData).forEach(function (value) {
        value.numberOfCommits = 0;
        value.issuesOpened = 0;
        value.issuesClosed = 0;
      });

      // Add commits
      this.state.commits.forEach(function (commit) {
        const teamMemberKeys = Object.keys(teamData).filter(function (teamMemberKey) {
          return commit.author_name.toLowerCase().includes(teamMemberKey);
        });
        if (teamMemberKeys.length === 1) {
          const teamMemberKey = teamMemberKeys[0];
          teamData[teamMemberKey].numberOfCommits = teamData[teamMemberKey].numberOfCommits + 1;
        }
      });

      // Add issues
      this.state.issues.forEach(function (issue) {
        const openedTeamMemberKeys = Object.keys(teamData).filter(function (teamMemberKey) {
          return issue.author.name.toLowerCase().includes(teamMemberKey);
        });

        if (openedTeamMemberKeys.length === 1) {
          const openedTeamMemberKey = openedTeamMemberKeys[0];
          teamData[openedTeamMemberKey].issuesOpened = teamData[openedTeamMemberKey].issuesOpened + 1;
        }
        const closedTeamMemberKeys = Object.keys(teamData).filter(function (teamMemberKey) {
          if (issue.closed_by === null) {
            return false;
          } else {
            return issue.closed_by.name.toLowerCase().includes(teamMemberKey);
          }
        });
        if (closedTeamMemberKeys.length === 1) {
          const closedTeamMemberKey = closedTeamMemberKeys[0];
          teamData[closedTeamMemberKey].issuesClosed = teamData[closedTeamMemberKey].issuesClosed + 1;
        }
      });

      totalGitLabStats = (<div>
                            <Row>
                              <Col className="text-center">
                                <h3>Total Stats</h3>
                              </Col>
                            </Row>
                            <Row>
                              <Col sm={4} className="text-center">Total Commits: {this.state.commits.length}</Col>
                              <Col sm={4} className="text-center">Total Issues: {this.state.issues.length}</Col>
                              <Col sm={4} className="text-center">Total Unit Tests: {Object.values(teamData).map(t => t.unitTests).reduce((a,b) => a + b)}</Col>
                           </Row>
                         </div>);
    }

    const GeneralTextWrapper = styled.div`
      text-wrap: normal;
      text-align: center;
      padding-top: 3%;
    `;

    const SectionTitle = styled.h1`
      text-align: center;
    `;

    return (
      <Container>
        <section style={sectionStyle}>
        <Row>
          <Col sm={6}>
            <GeneralTextWrapper>
              <h1>What Did We Do?</h1>
              <p>{whatWeDoText}</p>
            </GeneralTextWrapper>
          </Col>
          <Col sm={6}>
            <GeneralTextWrapper>
              <h1>Why Is This Important?</h1>
              <p>{motivationText}</p>
            </GeneralTextWrapper>
          </Col>
        </Row>

        <Row>
          <Container>
            <Row>
              <Col className="text-center">
                <SectionTitle>The Team</SectionTitle>
              </Col>
            </Row>
            <Row>
              <RenderCards maxPerRow={2} cards={Object.values(teamData).map(t => (<TeamMember {...t} />))} />
            </Row>
           {totalGitLabStats}
          </Container>
        </Row>

        <Row>
          <Container>
            <Row>
              <Col className="text-center">
                <SectionTitle>Tools Used</SectionTitle>
              </Col>
            </Row>
            <Row>
              <RenderCards maxPerRow={3} cards={toolsData.map(t => (<Tool {...t} />))} />
            </Row>
          </Container>
        </Row>

        <Row>
          <Container>
            <Row>
              <Col className="text-center">
                <SectionTitle>Data Sources</SectionTitle>
              </Col>
            </Row>
            <Row>
              <RenderCards maxPerRow={3} cards={dataSources.map(t => (<Data {...t} />))} />
            </Row>
          </Container>
        </Row>

        <Row>
          <Container>
            <Row>
              <Col className="text-center">
                <SectionTitle>Project Links</SectionTitle>
              </Col>
            </Row>
            <Row>
              <RenderCards maxPerRow={3} cards={linkData.map(t => (<ProjectLink {...t} />))} />
            </Row>
          </Container>
        </Row>
        </section>
      </Container>
    );
  }
}
