import { jsonPromise } from '../../../Misc/API.js';

function customerAPIFetch (modelName, pageNumber) {
    const endpoint = "https://api.livingendangerously.me/api";
    let params = new URLSearchParams();
    params.append("results_per_page", 9);
    params.append("page", pageNumber);
    const url = `${endpoint}/${modelName}?${params.toString()}`;
    return fetch(url).then(jsonPromise);
}

function customerAPIFetchAll (modelName) {
    return customerAPIFetch(modelName, 1).then((data) => {
        let total_pages = data.total_pages;
        let promises = [];
        for (var i = 2; i <= total_pages; i++) {
            promises.push(customerAPIFetch(modelName, i));
        }
        return Promise.all(promises).then((remainingData) => {
            let objects = data.objects;
            for (var i = 0; i < remainingData.length; i++) {
                let remaining = remainingData[i];
                for (var j = 0; j < remaining.objects.length; j++) {
                    objects.push(remaining.objects[j]);
                }
            }
            return objects;
        });
    });
}

export { customerAPIFetch, customerAPIFetchAll };
