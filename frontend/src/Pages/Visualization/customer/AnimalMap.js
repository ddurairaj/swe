import React, { Component } from 'react';
import * as d3 from "d3";
import makeCancelable from '../../../Misc/makeCancelable.js';
import { customerAPIFetchAll } from './API.js';
import Trump from "../../../Components/Trump.gif";
import Datamap from "react-datamaps";

class AnimalMap extends Component {

    constructor (props) {
        super(props);
        this.state = { data: null };
    }

    componentWillMount() {
        this.apiPromise_ = makeCancelable(customerAPIFetchAll("animals").then((data) => this.setState({data: data})));
    }

    render () {
        if (this.state.data === null) {
            return (
                <div>
                  <div style={{textAlign: "center"}}>
                    <img src={Trump} height="35%" width="35%" alt="rocket gif" style={{ marginTop: "1.5%"}} />
                    <p>
                      Loading...
                    </p>
                  </div>
                </div>);
        } else {
            console.log(this.state.data);
            const states = [
                "AZ",
                "CO",
                "DE",
                "FL",
                "GA",
                "HI",
                "ID",
                "IL",
                "IN",
                "IA",
                "KS",
                "KY",
                "LA",
                "MD",
                "ME",
                "MA",
                "MN",
                "MI",
                "MS",
                "MO",
                "MT",
                "NC",
                "NE",
                "NV",
                "NH",
                "NJ",
                "NY",
                "ND",
                "NM",
                "OH",
                "OK",
                "OR",
                "PA",
                "RI",
                "SC",
                "SD",
                "TN",
                "TX",
                "UT",
                "WI",
                "VA",
                "VT",
                "WA",
                "WV",
                "WY",
                "CA",
                "CT",
                "AK",
                "AR",
                "AL",
            ];

            let stateCount = {};
            for (let i = 0; i < states.length; i++) {
                let state = states[i];
                stateCount[state] = 0;
            }

            let animals = this.state.data;
            for (let i = 0; i < animals.length; i++) {
                if (animals[i].category === "Secure") continue;
                let currentStates = animals[i].states.map((s) => s.code);
                for (let j = 0; j < currentStates.length; j++) {
                    let currentState = currentStates[j];
                    stateCount[currentState] += 1;
                }
            }

            let maxCount = Object.values(stateCount).reduce((c, v) => c < v ? v : c, 0);

            let paletteScale = d3.scaleLinear()
                .domain([0, maxCount])
                .range(["#EFEFFF", "#02386F"]); // blue color

            let dataset = {};
            for (let i = 0; i < states.length; i++) {
                let state = states[i];
                dataset[state] = {
                    fillColor: paletteScale(stateCount[state]),
                    number: stateCount[state]
                };
            }

            console.log(dataset);

            return (
                <div>
                  <Datamap
                    width={1200}
                    height={1200}
                    scope="usa"
                    geographyConfig={
                        {
                            highlightBorderColor: '#bada55',
                            popupTemplate: (geography, data) =>
                                `<div class='hoverinfo'>${geography.properties.name}\nNumber of Not Secure Species: ${data.number}`,
                            highlightBorderWidth: 3
                          }
                      }
                    data={dataset}
                    labels />
               </div>
            );
        }
    }

}

export default AnimalMap;
