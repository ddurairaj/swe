import React, { Component } from 'react';
import Highlighter from 'react-highlight-words';

export default class TextHighlight extends Component {
  render() {
    let words = [];
    for (let i = 0; i < this.props.filterText.length; i++) {
      let filter = this.props.filterText[i];
      if (filter !== null && filter !== undefined) {
        words = words.concat(filter.toLowerCase().split(" "));
      }
    }

    const highlightStyle = {
      "background-color": "#a4e5a4"
    };

    return (
      <Highlighter searchWords={words} highlightStyle={highlightStyle} autoEscape={true} textToHighlight={this.props.text} caseSensitive={false}/>
    );
  }
}
