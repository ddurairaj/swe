import React, { Component } from 'react';
import { FacebookProvider, Page } from 'react-facebook';

export default class FacebookEmbed extends Component {
  render () {
    // TODO: this app id belongs to Buzzfeed News. maybe we should create our own app id
    return (
      <FacebookProvider appId="862012947269736">
        <Page href={`https://www.facebook.com/${this.props.profile}`} tabs="timeline" />
      </FacebookProvider>
    );
  }
}
