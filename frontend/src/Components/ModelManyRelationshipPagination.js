import React, { Component } from 'react';
import ModelPagination from './ModelPagination.js';

function doNothing() {}


export default class ModelManyRelationshipPagination extends Component {
  render () {

    const promiseArray = this.props.promiseArray;

    const renderData = (data) => {
      let PageComponent = this.props.pageContentComponent;
      return (<PageComponent data={data} filter={{}} />);
    };

    class FakePagination {
      constructor (pageSize, filter, useCompleteData) {
        this.pageSize = pageSize;
        this.filter = filter;
      }

      getPage (pageNumber) {
        let result = [];
        let offset = this.pageSize * (pageNumber - 1);
        for (let i = offset; i < promiseArray.length && i < offset + this.pageSize; i++) {
          result.push(promiseArray[i]);
        }

        return Promise.all(result);
      }

      totalPages () {
        return Math.ceil(promiseArray.length / this.pageSize);
      }
    }

    return (
      <ModelPagination
        model={this.props.model}
        paginationCtor={FakePagination}
        render={renderData}
        filter={{}}
        onFilterChange={doNothing}
        showFilter={false} />
    );
  }

}
