import React, { Component } from 'react';
import { TwitterTimelineEmbed } from 'react-twitter-embed';

export default class TwitterEmbed extends Component {
  render () {
    return ( <TwitterTimelineEmbed
             sourceType="profile"
             screenName={this.props.profile}
             options={{height: 400}}
             />);
  }
}
