import React, { Component } from 'react';
import { Container, Row, Col, Button, Input } from 'reactstrap';
import searchIcon from "./searchIcon.png";

export default class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: props.initialValue
    };
    this.onInputChange = this.onInputChange.bind(this);

  }

  onInputChange (nativeEvent) {
    this.setState({ searchValue: nativeEvent.target.value });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ searchValue: nextProps.initialValue });
  }

  render () {
    let searchValue = this.state.searchValue === null ? "" : this.state.searchValue;
    return (
      <Container fluid={true}>
        <Row noGutters={true}>
          <Col sm={11}>
            <Input
              type="text"
              placeholder="Search…"
              value={searchValue}
              onChange={this.onInputChange}
              />
          </Col>
          <Col sm={1}>
            <Button color="light" onClick={() => {
                return this.props.onChange(this.state.searchValue);
              }}><img src = {searchIcon} width="22" height="22" alt="searchIcon"></img></Button>
          </Col>
        </Row>
      </Container>
    );
  }
}
