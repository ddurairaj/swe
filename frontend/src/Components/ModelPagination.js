import React, { Component } from 'react';
import Pagination from 'react-js-pagination';
import makeCancelable from "../Misc/makeCancelable.js";
import Filter from './Filter.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import styled from 'styled-components/macro';
import './Pagination.css';
import Trump from "./Trump.gif";

export default class ModelPagination extends Component {
  // For pagination
  constructor(props) {
    super(props);
    let paginationObject;
    let numPerPage = props.numPerPage !== undefined && props.numPerPage !== null ? props.numPerPage : 9;
    if (props.paginationCtor !== null && props.paginationCtor !== undefined) {
      paginationObject = new props.paginationCtor(numPerPage, props.filter, true);
    } else {
      paginationObject = new props.model.Pagination(numPerPage, props.filter, true);
    }
    this.state = {
      pageData: null,
      totalPages: null,
      currentPage: null,
      paginationObject: paginationObject,
    };
    this.promise_ = null;
    this.handlePageChange = this.handlePageChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.filter !== this.props.filter || nextProps.model !== this.props.model) {
      let paginationObject;
      let numPerPage = nextProps.numPerPage !== undefined && nextProps.numPerPage !== null ? nextProps.numPerPage : 9;
      if (nextProps.paginationCtor !== null && nextProps.paginationCtor !== undefined) {
        paginationObject = new nextProps.paginationCtor(numPerPage, nextProps.filter, true);
      } else {
        paginationObject = new nextProps.model.Pagination(numPerPage, nextProps.filter, true);
      }
      this.setState({
        pageData: null,
        totalPages: null,
        currentPage: null,
        paginationObject: paginationObject,
      }, () => {
        this.handlePageChange(1);
      });
    }
  }

  componentDidMount () {
    this.handlePageChange(1);
  }

  componentWillUnmount () {
    if (this.promise_ !== null) {
      this.promise_.cancel();
      this.promise_ = null;
    }
  }


  // Handle navigation
  handlePageChange(pageNumber) {
    if (this.promise_ !== null) {
      this.promise_.cancel();
      this.promise_ = null;
    }

    let pageDataPromise = this.state.paginationObject.getPage(pageNumber);
    let totalPagesPromise = this.state.paginationObject.totalPages();


    this.setState({
      pageData: null
    }, () => {
      this.promise_ = makeCancelable(Promise.all([pageDataPromise, totalPagesPromise]).then(function ([pageData, totalPages]) {
        this.setState({
          pageData: pageData,
          totalPages: totalPages,
          currentPage: pageNumber,
        });
      }.bind(this)));
    });
  }

  render() {
    let elements = [];
    if (this.props.showFilter) {
      elements.push((<Filter key={0} schema={this.props.model.rawSchema()} filterObject={this.props.filter} onChange={this.props.onFilterChange} />));
    }
    if (this.state.pageData !== null) {
      elements.push((
        <div key={1}>
          {this.props.render(this.state.pageData)}
        </div>));
      elements.push((
        <Pagination
          key={2}
          nextLabel="Next"
          activePage={this.state.currentPage}
          pageRangeDisplayed={10}
          itemsCountPerPage={1}
          totalItemsCount={this.state.totalPages}
          innerClass="pagination justify-content-center"
          linkClass="page-link"
          itemClass="page-item"
          onChange={this.handlePageChange}
          />));
    } else {
      let CenteredDiv = styled.div`
        display: flex;
        justify-content: center;
      `;
      elements.push((
        <CenteredDiv key={1}>
            <div style={{textAlign: "center"}}>
                <img src={Trump} height="100%" width="100%" alt="rocket gif" style={{ marginTop: ".5%"}} />
                <p>
                    Loading...
                </p>
            </div>

        </CenteredDiv>
      ));
    }
    return (
      <div id="pagi">
        {elements}
      </div>
    );
  }
}
