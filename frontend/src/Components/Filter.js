import React, { Component } from 'react';
import Select from 'react-select';
import { Container, Row, Col, Button } from 'reactstrap';
import makeUserFriendlyLabel from '../Misc/makeUserFriendlyLabel.js';
import Search from './Search.js';
import './Card.css';

/*
 * A controllable Filter
 * Usage:
 * <Filter schema={schema}
 *         filterObject={filter}
 *         onChange={(newFilter) => console.log(newFilter)} />
 */

export default class Filter extends Component {

  fixedFilterableAttributes() {
    let attributes = this.props.schema.attributes;
    let filterableAttributes = Object.keys(attributes).reduce(function (filtered, key) {
      if (attributes[key].filterable && attributes[key].allowedValues !== undefined) {
        filtered[key] = attributes[key];
      }
      return filtered;
    }, {});
    return filterableAttributes;
  }

  openFilterableAttributes() {
    let attributes = this.props.schema.attributes;
    let filterableAttributes = Object.keys(attributes).reduce(function (filtered, key) {
      if (attributes[key].filterable && attributes[key].allowedValues === undefined) {
        filtered[key] = attributes[key];
      }
      return filtered;
    }, {});
    return filterableAttributes;
  }

  sortableAttributes() {
    let attributes = this.props.schema.attributes;
    let sortableAttributes = Object.keys(attributes).reduce(function (sorted, key) {
      if (attributes[key].sortable) {
        sorted[key] = attributes[key];
      }
      return sorted;
    }, {});
    return sortableAttributes;
  }

  render() {


    let dropdowns = [];

    const sortableAttributes = this.sortableAttributes();
    const sortableOptions = Object.keys(sortableAttributes).map((key) => {
      return { value: key, label: makeUserFriendlyLabel(key) };
    });

    if (sortableOptions.length > 0) {
      let selectedSortOption = null;
      if (this.props.filterObject.sort !== undefined) {
        const matchingEntry = sortableOptions.filter((entry) => entry.value === this.props.filterObject.sort);
        if (matchingEntry.length > 0) {
          selectedSortOption = matchingEntry[0];
        }
        // should pass assert(this.props.filterObject.order !== undefined);
      }

      let sortDropdown = (<Select value={selectedSortOption} options={sortableOptions} placeholder={"Sort by…"} onChange={(entry) => {
        this.props.onChange({...this.props.filterObject, sort: entry.value, order: "asc"});
      }}/>);

      if (this.props.filterObject.order !== undefined) {
        let selectedSortOrderOption = this.props.filterObject.order === "desc" ? { value: "desc", label: "Descending" } : { value: "asc", label: "Ascending" };
        let sortOrderOptions = [
          { value: "desc", label: "Descending" },
          { value: "asc", label: "Ascending" }
        ];

        let orderDropdown = (<Select value={selectedSortOrderOption} options={sortOrderOptions} placeholder={"Order by..."} onChange={(entry) => {
          this.props.onChange({...this.props.filterObject, order: entry.value});
        }}/>);


        dropdowns.push((
          <Container fluid={true}>
            <Row noGutters={true}>
              <Col sm={12}>
                {sortDropdown}
              </Col>
            </Row>
            <Row noGutters={true}>
              <Col sm={12}>
                {orderDropdown}
              </Col>
            </Row>
          </Container>
        ));
      } else {
        dropdowns.push((
          <Container fluid={true}>
            <Row noGutters={true}>
              <Col sm={12}>
                {sortDropdown}
              </Col>
            </Row>
          </Container>));
      }
    }


    const fixedFilterableAttributes = this.fixedFilterableAttributes();
    const fixedFilterableDropdowns = Object.keys(fixedFilterableAttributes).map((key, index) => {
      let options;
      let selectedOption = null;
      if (fixedFilterableAttributes[key].allowedValues.values !== undefined) {
        options = fixedFilterableAttributes[key].allowedValues.values.map((allowedValue) => {
          return { value: allowedValue, label: fixedFilterableAttributes[key].allowedValues.valueToLabel(allowedValue) };
        });
        if (this.props.filterObject[key] !== undefined) {
          selectedOption = { value: this.props.filterObject[key], label: fixedFilterableAttributes[key].allowedValues.valueToLabel(this.props.filterObject[key]) };
        }
      } else {
        // range
        let range = fixedFilterableAttributes[key].allowedValues.range;
        options = [];
        for (let i = range[0]; i < range[1]; i += range[2]) {
          options.push({
            label: `${i}-${i + range[2]}`,
            value: {
              "min": i,
              "max": i + range[2]
            }
          });
        }
        if (this.props.filterObject[key] !== undefined) {
          let index = (this.props.filterObject[key]["min"] - range[0]) / range[2];
          selectedOption = options[index];
        }
      }

      const onChange = (entry) => {
        let filterCopy = {...this.props.filterObject};
        filterCopy[key] = entry.value;
        this.props.onChange(filterCopy);
      };

      return (
        <Container key={index}>
          <Row noGutters={true}>
            <Col sm={12}>
              <Select value={selectedOption}
                      options={options}
                      placeholder={`${makeUserFriendlyLabel(key)}…`}
                      onChange={onChange} />
            </Col>
          </Row>
        </Container>
      );
    });

    dropdowns = dropdowns.concat(fixedFilterableDropdowns);


    const openFilterableAttributes = Object.keys(this.openFilterableAttributes());
    let searchElement = null;
    let dropdownColumnSize;
    if (openFilterableAttributes.length > 0) {
      dropdownColumnSize = Math.min(Math.floor(8 / dropdowns.length), 3);
      let searchColumnSize = 11 - (dropdownColumnSize * dropdowns.length);
      let possibleSearchValues = openFilterableAttributes
          .map((key) => this.props.filterObject[key])
          .filter((value) => value !== undefined);
      let searchValue = possibleSearchValues.length === 0 ? null : possibleSearchValues.reduce((accumulator, current) => {
        if (current !== null && current.length > accumulator) {
          return current;
        } else {
          return accumulator;
        }
      });

      const onChange = (newValue) => {
        let filterCopy = {...this.props.filterObject};
        for (let i = 0; i < openFilterableAttributes.length; i++) {
          if (newValue !== null) {
            filterCopy[openFilterableAttributes[i]] = newValue;
          } else {
            delete filterCopy[openFilterableAttributes[i]];
          }
        }
        this.props.onChange(filterCopy);
      };

      searchElement = (
        <Col sm={searchColumnSize}>
          <Search initialValue={searchValue} onChange={onChange} />
        </Col>
      );
    } else {
      dropdownColumnSize = Math.floor(11 / dropdowns.length);
    }

    const columns = dropdowns.map((dropdown, index) => {
      return (<Col sm={dropdownColumnSize} key={index}>
              {dropdown}
              </Col>);
    });

    return (
      <Container className="wrapper">
        <Row noGutters={true}>
          <Col sm={1}>
            <Button color="danger" onClick={() => this.props.onChange({}) }>Reset</Button>
          </Col>
          {columns}
          {searchElement}
        </Row>
      </Container>);
  }
}
